# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20090509041132) do

  create_table "maps", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.integer  "initial_credits"
    t.integer  "income_credits"
    t.integer  "max_players"
    t.integer  "width"
    t.integer  "height"
    t.integer  "version"
    t.integer  "ratings_count"
    t.integer  "area"
    t.string   "size"
    t.string   "cached_tag_list", :default => ""
    t.string   "thumbnail_url"
    t.string   "preview_url"
    t.string   "preview_size",    :default => "250x250"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "players", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "points"
    t.integer  "wins"
    t.integer  "losses"
    t.integer  "draws"
    t.integer  "credits_spent"
    t.integer  "bases_captured"
    t.boolean  "pro",               :default => false
    t.boolean  "ready_to_play",     :default => false
    t.boolean  "on"
    t.string   "profile_image_url"
    t.string   "profile_text"
    t.string   "profile_url"
    t.integer  "games_count",       :default => 0
    t.string   "games"
    t.string   "prefers"
    t.string   "preferred_by"
    t.string   "favorite_maps"
    t.string   "favorite_units"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "weewar_games", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
