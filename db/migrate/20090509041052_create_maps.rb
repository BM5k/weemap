class CreateMaps < ActiveRecord::Migration
  def self.up
    create_table :maps do |t|
      t.string  :name
      t.text    :description

      t.integer :created_by
      t.integer :initial_credits
      t.integer :income_credits
      t.integer :max_players
      t.integer :width
      t.integer :height
      t.integer :version
      t.integer :ratings_count
      t.integer :area

      t.string  :size
      t.string  :cached_tag_list, :default => ""
      t.string  :thumbnail_url 
      t.string  :preview_url 	
      t.string  :preview_size, 	  :default => "250x250"

      t.timestamps
    end
  end

  def self.down
    drop_table :weewar_maps
  end
end
