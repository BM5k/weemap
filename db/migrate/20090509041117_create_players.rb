class CreatePlayers < ActiveRecord::Migration
  def self.up
    create_table :players do |t|
      t.integer :user_id
      t.string  :name
      t.integer :points
      t.integer :wins
      t.integer :losses
      t.integer :draws
      t.integer :credits_spent
      t.integer :bases_captured
      t.boolean :pro,             :default => false
      t.boolean :ready_to_play,   :default => false
      t.boolean :on
      t.string  :profile_image_url
      t.string  :profile_text
      t.string  :profile_url
      t.integer :games_count,     :default => 0
      t.string  :games
      t.string  :prefers
      t.string  :preferred_by
      t.string  :favorite_maps
      t.string  :favorite_units

      t.timestamps
    end
  end

  def self.down
    drop_table :players
  end
end
