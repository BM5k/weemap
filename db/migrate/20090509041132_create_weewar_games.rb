class CreateWeewarGames < ActiveRecord::Migration
  def self.up
    create_table :weewar_games do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :weewar_games
  end
end
