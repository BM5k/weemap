class Player < ActiveRecord::Base
  acts_as_weewar

# associations

  has_many :maps, :foreign_key => :created_by

# other crap

  attr_accessor :map_list

  serialize :games
  serialize :prefers
  serialize :preferred_by

private

  def legacy_map
    { :profile_image_url => 'profile_image',
      :name              => 'name',
      :profile_url       => 'profile',
      :profile_text      => 'profile_text',
      :points            => 'points',
      :draws             => 'draws',
      :wins              => 'victories',
      :losses            => 'losses',
      :bases_captured    => 'bases_captured',
      :credits_spent     => 'credits_spent',
      :games_count       => 'games_running'
    }
  end

  def self.request_url
    "#{Weewar::Api::BASE_URL}users"
  end

  def request_url
    # TODO: user/#{id} doesn't work anymore, always use user/#{name}.
    # "#{Weewar::Api::BASE_URL}user/#{id.blank? ? name : id}"
    "#{Weewar::Api::BASE_URL}user/#{name}"
  end

  def translated_legacy_map
    # TODO: the api response no longer contains preferred_by
    # :preferred_by   => imported_data['preferred_by'     ].values.flatten,
    # imported_data['maps']
    super.merge( :on  => true_or_false(imported_data['on']),
      :pro            => imported_data['account_type'] == 'Pro' ? true : false,
      :ready_to_play  => true_or_false(imported_data['ready_to_play']),
      :games          => imported_data['games'            ].values.flatten,
      :prefers        => imported_data['preferred_players'].values.flatten,
      :favorite_units => imported_data['favorite_units'   ].values.flatten,
      :map_list       => imported_data['maps'             ].values.flatten
    )
  end

end

