class Map < ActiveRecord::Base
  acts_as_weewar

# associations
  belongs_to :player

# callbacks
  before_update :update_size

# validations

# other crap

  def next
    Map.first(:conditions => ['id > ?', id], :order => "id asc")
  end
   
  def previous
    Map.first(:conditions => ['id < ?', id], :order => "id desc")
  end

private

  def legacy_map
    { :name             => 'name',
      :version          => 'revision',
      :initial_credits  => 'initial_credits',
      :income_credits   => 'per_base_credits',
      :width            => 'width',
      :height           => 'height',
      :max_players      => 'max_players',
      :thumbnail_url    => 'thumbnail',
      :preview_url      => 'preview'
    }
  end

  def self.request_url
    "#{Weewar::Api::BASE_URL}maps"
  end

  def request_url
    "#{Weewar::Api::BASE_URL}map/#{id}"
  end

  def update_size
    return unless width && height
    self.size = "#{width}x#{height}"
    self.area = width * height
  end

  # def self.translated_legacy_map
  #   super.merge(:creator => Player.find_or_create_by_name(data['creator']))
  # end
end
