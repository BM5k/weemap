require 'open-uri'

module Weewar
  module Api
    BASE_URL = 'http://weewar.com/api1/'

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      # uncomment this block & use acts_as_weewar in your models to include instance methods
      def acts_as_weewar(options = {})
        include InstanceMethods
      end

      def build!(id)
        item = find_or_initialize_by_id(id)
        item.update_from_api if item.new_record?
        item
      end

      def update_all_from_api
        all.select(&:update_from_api).collect(&:id)
      end
    end

    module InstanceMethods
      def api_request
        open(request_url).read
      end

      def imported_data
        Hash.from_xml(api_request).values.first
      end

      def update_from_api
        update_attributes(translated_legacy_map)
        self
      end

      def true_or_false(value)
        value == "true" ? true  : false
      end

    private

      def request_url
        # the request_url should be a string with the relative path to request for the given resource
        raise NotImplementedError.new("Override request_url in #{self.to_s.downcase}.rb!")
      end

      def legacy_map
        # the legacy map is a hash, the keys are symbols for your model's attributes; their values
        # indicate the legacy field names
        # ie { :new_column_one => 'legacy_column_one', :new_column_two => 'legacy_column_two' }
        raise NotImplementedError.new("Override legacy_map in #{self.to_s.downcase}.rb!")
      end

      def translated_legacy_map
        # override this method in your model for more advanced mapping
        Hash[*legacy_map.map {|new_column, legacy_column| [new_column, imported_data[legacy_column]]}.flatten]
      end
    end

  end
end
