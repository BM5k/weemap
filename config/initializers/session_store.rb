# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_weemap-new_session',
  :secret      => 'c45ba5fe8b00ac3321470b7faed48c69781e28bc1fd846f4b510a81368a3aaf2d1876281cf1f005e51bab7eeac17e33cc08b315cf5299a155de42de02002505b'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
