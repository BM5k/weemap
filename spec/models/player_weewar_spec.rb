require File.dirname(__FILE__) + '/../spec_helper'
require File.dirname(__FILE__) + '/weewar_spec_data'

# weewar players api interaction

describe Player, '.update_all_from_api' do

  before :all do
    @player = Player.new
  end

  before :each do
    @player.stub!(:update_from_api)
  end

  it 'calls update_from_api for each player' do
    Player.should_receive(:all).and_return([@player])
    Player.update_all_from_api
  end

end

# weewar player api interaction

describe Player, '.request_url' do
  it 'returns the weewar api url for all players' do
    Player.send(:request_url).should == "#{Weewar::Api::BASE_URL}users"
  end
end

describe Player, '#request_url' do
  it 'returns the weewar api url for this player when name is not set' do
    pending('API Support broken')
    player = Player.new
    player.id = 12345
    player.send(:request_url).should == "#{Weewar::Api::BASE_URL}user/12345"
  end

  it 'returns the weewar api url for this player when name is set and id is not' do
    player = Player.new
    player.name = 'foo'
    player.send(:request_url).should == "#{Weewar::Api::BASE_URL}user/foo"
  end
end

describe Player, '#legacy_map' do
  before :all do
    @legacy_map = Player.new.send(:legacy_map)
  end

  it 'includes name' do
    @legacy_map.keys.should   include(:name)
    @legacy_map.values.should include('name')
  end

  it 'includes profile' do
    @legacy_map.keys.should   include(:profile_url)
    @legacy_map.values.should include('profile')
  end

  it 'includes profile_image' do
    @legacy_map.keys.should   include(:profile_image_url)
    @legacy_map.values.should include('profile_image')
  end

  it 'includes profile_text' do
    @legacy_map.keys.should   include(:profile_text)
    @legacy_map.values.should include('profile_text')
  end

  it 'includes points' do
    @legacy_map.keys.should   include(:points)
    @legacy_map.values.should include('points')
  end

  it 'includes draws' do
    @legacy_map.keys.should   include(:draws)
    @legacy_map.values.should include('draws')
  end

  it 'includes victories' do
    @legacy_map.keys.should   include(:wins)
    @legacy_map.values.should include('victories')
  end

  it 'includes losses' do
    @legacy_map.keys.should   include(:losses)
    @legacy_map.values.should include('losses')
  end

  it 'includes bases_captured' do
    @legacy_map.keys.should   include(:bases_captured)
    @legacy_map.values.should include('bases_captured')
  end

  it 'includes credits_spent' do
    @legacy_map.keys.should   include(:credits_spent)
    @legacy_map.values.should include('credits_spent')
  end

  it 'includes games_running' do
    @legacy_map.keys.should   include(:games_count)
    @legacy_map.values.should include('games_running')
  end
end

describe Player, '#translated_legacy_map' do
  it 'overrides the default translated_legacy_map' do
    pending
    player = Player.new
    player.stub!(:imported_data).and_return(hashed_player_data)
  end
end

def translated_hash
  {
    :credits_spent     => "1000000",
    :points            => "2502",
    :games_count       => "2",
    :pro               =>  true,
    :draws             => "0",
    :name              => "Test",
    :ready_to_play     => false,
    :wins              => "46",
    :on                =>  false,
    :games             => ["3210", "12345"],
    :profile_url       => "http://weewar.com/user/Test",
    :favorite_units    => [{"code" => "lightInfantry"}, {"code" => "lighttank"}],
    :losses            => "1",
    :profile_image_url => "http://weewar.com/images/profile/12345_wrzemn.png",
    :bases_captured    => "5",
    :prefers           => [{"name" => "Jake", "id" => "12321"}, {"name" => "Ellwood", "id" => "40404"}],
    :profile_text      => "I rock!"
  }
end
