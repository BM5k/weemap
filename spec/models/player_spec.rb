require File.dirname(__FILE__) + '/../spec_helper'

describe Player, 'associations' do
  it 'has_many :maps' do
    Player.new.should have_many(:maps)
  end
end

# describe Player, 'validations' do
# end
