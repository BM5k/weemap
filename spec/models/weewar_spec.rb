require File.dirname(__FILE__) + '/../spec_helper'
require File.dirname(__FILE__) + '/weewar_spec_data'

API_TEST_FILE = File.join(RAILS_ROOT, 'spec', 'data', 'api-test.xml')

class WeewarTest
  require 'weewar'
  include Weewar::Api
  acts_as_weewar

  attr_accessor :new_field_one, :field_two

  def request_url
    API_TEST_FILE
  end

private

  def legacy_map
    { :new_field_one => 'legacy_field_one', :field_two => 'field_two' }
  end

end

class WeewarTestError
  require 'weewar'
  include Weewar::Api
  acts_as_weewar

  #this class does not implement anything!
end

describe WeewarTest, '#api_request' do
  before :all do
    @weewar = WeewarTest.new
  end

  it 'calls request url on itself' do
    @weewar.should_receive(:request_url).and_return(API_TEST_FILE)
    @weewar.api_request
  end

  it 'opens the specified url' do
    @weewar.should_receive(:open).and_return(File.open(API_TEST_FILE))
    @weewar.api_request
  end

  it 'reads text from the open stream' do
    @weewar.api_request.should == xml_test_data
  end
end

describe WeewarTest, '#imported_data' do
  before :all do
    @weewar = WeewarTest.new
  end

  it 'calls api_request' do
    @weewar.should_receive(:api_request).and_return(xml_test_data)
    @weewar.imported_data
  end

  it 'creates a hash from the received xml' do
    @weewar.imported_data.should == hashed_test_data
  end
end

describe WeewarTest, '#translated_legacy_map' do
  before :all do
    @weewar = WeewarTest.new
  end

  it 'translates the legacy map' do
    @weewar.send(:translated_legacy_map).should == { :new_field_one => "first field", :field_two => "second field" }
  end
end

describe WeewarTest, '#update_from_api' do
  before :all do
    @weewar = WeewarTest.new
    @weewar.stub!(:update_attributes)
  end

  it 'calls translated_legacy_map' do
    @weewar.should_receive(:translated_legacy_map)
    @weewar.update_from_api
  end

  it 'calls update_attributes with the translated legacy map' do
    @weewar.should_receive(:update_attributes)
    @weewar.update_from_api
  end
end

describe WeewarTest, '.true_or_false' do
  before :all do
    @weewar = WeewarTest.new
  end

  it 'returns true when passed the string true' do
    @weewar.true_or_false('true').should == true
  end

  it 'returns true when passed the string true' do
    @weewar.true_or_false('false').should == false
  end
end

describe WeewarTest, '#build' do
  before :all do
    @weewar = WeewarTest.new
  end

  before :each do
    WeewarTest.stub!(:find_or_initialize_by_id).and_return(@weewar)

    @weewar.stub!(:new_record?).and_return(:false)
    @weewar.stub!(:update_attributes)
    @weewar.stub!(:update_from_api)
  end

  it 'calls find_or_initialize_by_id' do
    WeewarTest.should_receive(:find_or_initialize_by_id).with(12345).and_return(@weewar)
    WeewarTest.build!(12345)
  end

  it 'calls update_from_api' do
    @weewar.should_receive(:update_from_api)
    WeewarTest.build!(12345)
  end

  it 'returns the item found' do
    WeewarTest.build!(12345).should == @weewar
  end
end
