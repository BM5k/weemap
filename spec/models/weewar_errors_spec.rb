require File.dirname(__FILE__) + '/../spec_helper'

class WeewarTestError
  require 'weewar'
  include Weewar::Api
  acts_as_weewar

  #this class does not implement anything!
end

describe WeewarTestError do
  it 'request_url raises a NotImplementedError' do
    lambda { WeewarTestError.new.send(:request_url) }.should raise_error(NotImplementedError)
  end

  it 'legacy_map raises a NotImplementedError' do
    lambda { WeewarTestError.new.send(:legacy_map) }.should raise_error(NotImplementedError)
  end
end
