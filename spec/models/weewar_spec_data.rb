def xml_test_data
<<-EOF
<?xml version="1.0" encoding="UTF-8"?>
<weewar_test>
  <legacy_field_one>first field</legacy_field_one>
  <field_two>second field</field_two>
</weewar_test>
EOF
end

def hashed_test_data
  Hash.from_xml(xml_test_data).values.first
end

def player_xml
<<-EOF
<?xml version="1.0" encoding="UTF-8"?>
<user name="Test" id="12345">
  <points>2502</points>
  <profile>http://weewar.com/user/Test</profile>
  <profileImage>http://weewar.com/images/profile/12345_wrzemn.png</profileImage>
  <draws>0</draws>
  <victories>46</victories>
  <losses>1</losses>
  <accountType>Pro</accountType>
  <on>false</on>
  <readyToPlay>false</readyToPlay>
  <gamesRunning>2</gamesRunning>
  <lastLogin>2008-01-19 06:36:57.0</lastLogin>
  <basesCaptured>5</basesCaptured>
  <creditsSpent>1000000</creditsSpent>
  <profileText>I rock!</profileText>
  <favoriteUnits>
    <unit code="lightInfantry" />
    <unit code="lighttank" />
  </favoriteUnits>
  <preferredPlayers>
    <player name="Jake"    id="12321" />
    <player name="Ellwood" id="40404" />
  </preferredPlayers>
  <games>
    <game name="Bogus Game 1">3210</game>
    <game name="Bogus Game 2">12345</game>
  </games>
  <maps>
    <map>0123</map>
    <map>4567</map>
    <map>8910</map>
    <map>1112</map>
  </maps>
</user>
EOF
end

def hashed_player_data
  Hash.from_xml(player_xml).values.first
end
