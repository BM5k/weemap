require File.dirname(__FILE__) + '/../spec_helper'

describe Map, '.request_url' do
  it 'returns the weewar api url for all maps' do
    Map.send(:request_url).should == "#{Weewar::Api::BASE_URL}maps"
  end
end

describe Map, '#request_url' do
  it 'returns the weewar api url for this map' do
    map = Map.new
    map.id = 12345
    map.send(:request_url).should == "#{Weewar::Api::BASE_URL}map/12345"
  end
end

describe Map, '#legacy_map' do
  before :all do
    @legacy_map = Map.new.send(:legacy_map)
  end

  it 'does not include terrains' do
    @legacy_map.keys.should_not   include(:terrains)
    @legacy_map.values.should_not include('terrains')
  end

  it 'includes name' do
    @legacy_map.keys.should   include(:name)
    @legacy_map.values.should include('name')
  end

  it 'includes revision' do
    @legacy_map.keys.should   include(:version)
    @legacy_map.values.should include('revision')
  end

  it 'includes initial_credits' do
    @legacy_map.keys.should   include(:initial_credits)
    @legacy_map.values.should include('initial_credits')
  end

  it 'includes per_base_credits' do
    @legacy_map.keys.should   include(:income_credits)
    @legacy_map.values.should include('per_base_credits')
  end

  it 'includes width' do
    @legacy_map.keys.should   include(:width)
    @legacy_map.values.should include('width')
  end

  it 'includes height' do
    @legacy_map.keys.should   include(:height)
    @legacy_map.values.should include('height')
  end

  it 'includes max_players' do
    @legacy_map.keys.should   include(:max_players)
    @legacy_map.values.should include('max_players')
  end

  it 'includes thumbnail' do
    @legacy_map.keys.should   include(:thumbnail_url)
    @legacy_map.values.should include('thumbnail')
  end

  it 'includes preview' do
    @legacy_map.keys.should   include(:preview_url)
    @legacy_map.values.should include('preview')
  end
end
