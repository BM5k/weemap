require File.dirname(__FILE__) + '/../spec_helper'

describe Map, 'associations' do
  it 'belongs_to :player' do
    Map.new.should belong_to(:player)
  end
end

# describe Map, 'validations' do
# end

describe Map, '.update_size' do
  it 'returns nil if width is not set' do
    Map.new(:width => nil).send(:update_size).should be_nil
  end

  it 'returns nil if height is not set' do
    Map.new(:height => nil).send(:update_size).should be_nil
  end

  it 'sets the size if the map has a height and width' do
    map = Map.new(:height => 5, :width => 10)
    map.send(:update_size)
    map.size.should == '10x5'
  end

  it 'sets the area if the map has a height and width' do
    map = Map.new(:height => 5, :width => 10)
    map.send(:update_size)
    map.area.should == 50
  end
end

describe Map, '#next' do
  before :all do
    @first = Map.create!
    @last  = Map.create!
  end

  it 'returns the next map' do
    @first.next.should == @last
  end

  it 'returns the next map' do
    @last.previous.should == @first
  end

  after :all do
    @first.destroy
    @last.destroy
  end
end